---
bbibliography: ["biostats.bib","R_packages.bib"]
---
# SIR Models

Last Update: `r Sys.Date()`

## Background Information

The Susceptible-Infected-Recovered (SIR) Model is a deterministic model used to understand the rates of infectious diseases in a population. The idea is that a closed-population, which means no one is entering or leaving the population, can be divided up into three groups: susceptible, infected, and recovered. An individual will start in the susceptible population where then can get infected from a disease. This individual will then move to the infectious group the moment they contract the disease. Lastly, the individual will move to recovered population, where they can no longer get infected or infected the susceptible population. As the disease progresses in a population, a number of individuals move from one group to another. This movement of individuals at a certain time can be modeled. 

The model looks at how the three populations, susceptible ($S$), infected ($I$), and recovered ($R$), change in time when a new disease is introduced. With a new disease, the susceptible population will decrease over time because individuals become ill and move to the infected population. The infected population is more dynamic where there are individuals both entering and exiting the population. This is because individuals are becoming ill and then they soon recover after a some time. Lastly, the recovered population will always increase because individuals are recovering from the disease, assuming they cannot be reinfected with the same disease. These population are then modeled with the SIR model to understand how many individuals belong to each population. 

The SIR model uses a set of ordinary differential equation representing the susceptible rate, infected rate, and recovered rate:

\[
\frac{dS}{dt}=-\beta S I,
(\#eq:S1)
\]

\[ 
\frac{dI}{dt}=\beta S I-\gamma I,
(\#eq:I1)
\]

and

\[ 
\frac{dR}{dt}=\gamma I,
(\#eq:R1)
\]

where $\beta$ represent the rate a susceptible population becomes infected and $\gamma$ represents the rate the infected population recovers, respectively. Solving these differential equations will allow us to determine how many individuals belong to each population. In more detail, equation \@ref(eq:S1) shows the number of individuals leaving the susceptible population is based on the number of susceptible individuals and the number of Infected individuals. Additionally, equation \@ref(eq:S1) is always negative. Equation \@ref(eq:I1) show the how the infection rate is a function of how many individuals become infected and how many individuals recover. The infected rate may be either positive or negative depending on the number of individuals are still susceptible. Lastly, equation \@ref(eq:R1) shows the recovery rate is dependent only on the number of individuals that are infected. The recovery rate is always positive.

The parameters $\beta$ and $\gamma$ are necessary to understand how the disease spreads in a population. The inverse of parameter $\gamma$ can be thought as the number of days an individual is infectious; therefore, $\gamma$ is inverse of the number of days an individual is infectious. The parameter $\beta$ can be obtained with the reproduction number ($R_0$), population size ($N$), and recovery rate ($\gamma$):

\[
\beta=\frac{R_0}{N\gamma},
(\#eq:R0)
\]

where the reproduction number $R_0$ is defined as the average number of susceptible individuals will be infected from an infectious individual.

Equations \@ref(eq:S1), \@ref(eq:I1), and \@ref(eq:R1) are a systems of ordinary differential equations that can be solved numerically. Solving numerically means that the the values $S$, $I$, $R$ can be found for a particular time of t. To solve the system of differential equations, we will use the classical Runge-Kutta 4th Order Integration.

One major assumption with the SIR model is that the population is fixed. This means that no new members enter the population.

## COVID-19 Information

While this is a rapidly developing disease where my estimates are most likely wrong, there are initial estimates about the reproduction number $R_0$ and the number of days an individual is infected. One study has found that, before any intervention, the average $R_0$ is 2.35 [@kucharskiEarlyDynamicsTransmission2020]. Once an intervention is applied, the average $R_0$ drops to 1.05[@kucharskiEarlyDynamicsTransmission2020]. The number days an individual is infected may be more challenging; however, making many assumptions, let say infectious period is the incubation period when the infected individual is interacting with the public. The current estimate of the incubation period for COVID-19 is 5.1 days [@backerIncubationPeriod20192020].

While there are modern models that better predict the "flattening of the curve" effect with COVID-19, the SIR model is a simple way to show how reducing the reproduction number has an effect on the outbreak. 


## No Restrictions Simulation

The R-Code provided here was adapted from two R Bloggers posts that go in more detail: [https://www.r-bloggers.com/flatten-the-covid-19-curve/](https://www.r-bloggers.com/flatten-the-covid-19-curve/) and [https://www.r-bloggers.com/sir-model-with-desolve-ggplot2/](https://www.r-bloggers.com/sir-model-with-desolve-ggplot2/).

Conducting a SIR model is fairly straight forward. First, we will use the `tidyverse` for plotting and basic data manipulation and `deSolve` package to solve our systems differential equations [@R-tidyverse,@deSolve2010].

```{r,message=F}
library(tidyverse)
library(deSolve)
```

The next thing is to create a function to represent the SIR model.

```{r}
sir <- function(t, y, parms) {
  beta <- parms[1]
  gamma <- parms[2]
  S <- y[1]
  I <- y[2]
  R <- y[3]
  return(list(c(dS = -beta * S * I, dI = beta * S * I - gamma * I,dR=gamma*I)))
}
```

I created am R function called `sir` using the function `function()`. The `sir()` function takes the arguments `t` for time, `y` is a vector for the number of susceptible, infectious, and recovered individuals, respectively, and `parms` is a vector representing the values of $\beta$ and $\gamma$

Next, let's set up the parameters for the model. Create R variables `N`, `gamma`, `R0`, and  `beta` to represent the size of the population, inverse infection period, reproduction number, and infectious rate.

The last thing is to set up vector of time points for 500 days

```{r}
# Population size 
(N <- 1e6) 
#Inverse infection period
(gamma <- 1/5.1) 
#Reproduction number
(R0<-2.35)
#Infection rate
(beta<-R0/N*gamma)
#Time points
max_time <- 2*365
times <- seq(0, max_time, by=0.1)
```

The systems of equations can be solved using the `rk4()` function from the `deSolve` R Package. The `rk4()` function needs the vector `y` to be specified. Here, we will specify `y` to be equal to `c(N-10,10,0)`, where `c()` creates the vector, `N-10` represents the number susceptible at time 0, `10` represents the number of individual infectious at time 0, and `0` represents the number recovered at time 0.

```{r}
# Solving ODE system
ode_soln_1 <- rk4(y = c(N-10,10,0), times = times, func = sir, parms = c(beta, gamma)) 
```

The `rk4()` function provides a matrix of the number of individuals in each different state for a specific time. Using this matrix, we can plot how many individuals are susceptible, infectious, and recovered over time. This can be done by converting the matrix to a tibble and then use it with `ggplot()`.

```{r}
ode_tbl_1<-ode_soln_1 %>% as.data.frame() %>% setNames(c("t", "S", "I","R"))
ggplot(data = ode_tbl_1)+geom_line(aes(x=t,y=S,color="Susceptible"))+geom_line(aes(x=t,y=I,color="Infected"))+geom_line(aes(x=t,y=R,color="Recovered"))+theme_bw()+xlab("Time in Days")+ylab("Number of individuals")+theme(legend.title = element_blank()) 
```

To identify when the peak infection occur, use `which.max()` function on `ode_tbl_1`.

```{r}
(time.index_1 <- which.max(ode_tbl_1$I))
(peak_day_1<-ode_tbl_1$t[time.index_1])
```

The peak infection period occurred around day `r round(peak_day_1)`.

Additionally, we can find out how number of individuals and proportion of population becoming ill.

```{r}
(sick_1<-round(max(ode_tbl_1$R)))
(prop_1<-sick_1/N)
```

Notice that almost `r round(prop_1,3)*100`\% of the population became ill.

## Moderate Restriction Simulation

Now, let's say that restrictions were in place to limit the interaction with individuals, $R_0$ will drop because individuals will no longer interact with each other. We will create a SIR Model with a $R_0$ of 1.5 and keep the other parameters the same.

```{r,message=F}
# Population size 
(N <- 1e6) 
#Inverse infection period
(gamma <- 1/5.1) 
#Reproduction number
(R0<-1.5)
#Infection rate
(beta<-R0/N*gamma)
#Time points
max_time <- 2*365
times <- seq(0, max_time, by=0.1)
# Solving ODE system
ode_soln_2 <- rk4(y = c(N-100,100,0), times = times, func = sir, parms = c(beta, gamma)) %>% as.data.frame()
ode_tbl_2<-ode_soln_2 %>% setNames(c("t", "S", "I","R"))
```


```{r}
ggplot(data = ode_tbl_2)+geom_line(aes(x=t,y=S,color="Susceptible"))+geom_line(aes(x=t,y=I,color="Infected"))+geom_line(aes(x=t,y=R,color="Recovered"))+theme_bw()+xlab("Time in Days")+ylab("Number of Individuals")+theme(legend.title = element_blank()) 
```

Notice how much the number of infected was reduced. Now let's generate some statistics about the curve.

```{r}
(time.index_2 <- which.max(ode_tbl_2$I))
(peak_day_2<-ode_tbl_2$t[time.index_2])
(sick_2<-round(max(ode_tbl_2$R)))
(prop_2<-sick_2/N)
```

The peak infection period occurred in `r round(peak_day_2)`. The proportion infected with the disease was `r round(prop_2,3)*100`\% when $R_0$ is 1.5.


## Severe Restriction Simulation

Now, let's see how severe restriction to limit the interaction with individuals affects the model. We will create a SIR Model with a $R_0$ of 1.05 and keep the other parameters the same.

```{r,message=F}
# Population size 
(N <- 1e6) 
#Inverse infection period
(gamma <- 1/5.1) 
#Reproduction number
(R0<-1.05)
#Infection rate
(beta<-R0/N*gamma)
#Time points
max_time <- 2*365
times <- seq(0, max_time, by=0.1)
# Solving ODE system
ode_soln_3 <- rk4(y = c(N-100,100,0), times = times, func = sir, parms = c(beta, gamma)) %>% as.data.frame()
ode_tbl_3<-ode_soln_3 %>% setNames(c("t", "S", "I","R"))
```


```{r}
ggplot(data = ode_tbl_3)+geom_line(aes(x=t,y=S,color="Susceptible"))+geom_line(aes(x=t,y=I,color="Infected"))+geom_line(aes(x=t,y=R,color="Recovered"))+theme_bw()+xlab("Time in Days")+ylab("Number of Individuals")+theme(legend.title = element_blank()) 
```

As we can see, the majority of the population does not ge infected. Let's identify other statistics for this model.

```{r}
(time.index_3 <- which.max(ode_tbl_3$I))
(peak_day_3<-ode_tbl_3$t[time.index_3])
(sick_3<-round(max(ode_tbl_3$R)))
(prop_3<-sick_3/N)
```

The peak infection period occurred in `r round(peak_day_3)`. The proportion infected with the disease was `r round(prop_3,3)*100`\% when $R_0$ is 1.1.



## Moderate Restrictions Removed Early

When restrictions are removed early, the curve goes back up. For example, The peak in the moderate restriction happened at day `r round(peak_day_2)`. If the restrictions were lifted soon after, a secondary peak can be seen. This peak may be worse than the first peak. For example, once the restrictions are removed the reproduction number may jump back up at to 2.35. Let's see what happens if restriction are removed at day 90.


```{r,message=F}
sir.adj <- function(t, y, parms) {
  beta <- ifelse(t<90,parms[1],parms[2])
  gamma <- parms[3]
  S <- y[1]
  I <- y[2]
  R <- y[3]
  return(list(c(dS = -beta * S * I, dI = beta * S * I - gamma * I,dR=gamma*I)))
}

# Population size 
(N <- 1e6) 
#Inverse infection period
(gamma <- 1/5.1) 
#Reproduction number
(R0<-1.5)
#Infection rate restriction
(beta.a<-R0/N*gamma)
#Infection rate restriction removed
(R0<-2.35)
(beta.b<-R0/N*gamma)


#Time points
max_time <- 2*365
times <- seq(0, max_time, by=0.1)
# Solving ODE system
ode_soln_4 <- rk4(y = c(N-100,100,0), times = times, func = sir.adj, parms = c(beta.a, beta.b, gamma)) %>% as.data.frame()
ode_tbl_4<-ode_soln_4 %>% setNames(c("t", "S", "I","R"))
```

```{r}
ggplot(data = ode_tbl_4)+geom_line(aes(x=t,y=S,color="Susceptible"))+geom_line(aes(x=t,y=I,color="Infected"))+geom_line(aes(x=t,y=R,color="Recovered"))+theme_bw()+xlab("Time in Days")+ylab("Number of Individuals")+theme(legend.title = element_blank())+geom_vline(xintercept=90)
```

```{r}
(time.index_4 <- which.max(ode_tbl_4$I))
(peak_day_4<-ode_tbl_3$t[time.index_4])
(sick_4<-round(max(ode_tbl_4$R)))
(prop_4<-sick_4/N)
```

Notice that a secondary peak comes 9 days after, and the proportion ill-patients jumps to `r round(prop_4,2)`.


## No vs Moderate vs Severe Restriction vs Moderate/Removed

The three previous sections showed two different SIR Models. Now let's plot the SIR model to demonstrate the power of applying these restrictions. We will plot the infection rates side-by-side.

```{r,message=F}
ggplot(data=ode_tbl_1)+geom_line(aes(x=t,y=I,color="No Restriction"))+
  geom_line(data=ode_tbl_2,aes(x=t,y=I,color="Moderate Restriction"))+
  geom_line(data=ode_tbl_3,aes(x=t,y=I,color="Severe Restriction"))+
  geom_line(data=ode_tbl_4,aes(x=t,y=I,color="Moderate Restriction then Removed"))+
  theme_bw()+xlab("Time in Days")+ylab("Number of Individuals")+
  theme(legend.title = element_blank()) 
```


Notice how the severe restriction dramatically reduces the number of infectious cases compared to the other two. 
The proportion infected with the disease with no restriction is `r round(prop_1,3)*100`\% when $R_0$ is 2.35. The proportion infected with the disease with no restriction is `r round(prop_2,3)*100`\% when $R_0$ is 1.5. The proportion infected with the disease with no restriction is `r round(prop_3,3)*100`\% when $R_0$ is 1.05.

As we can see when we lower the interaction an infected person has on the susceptible population, the proportion decreases. This is the "flatten the curve" effect that public health officials are trying to achieve. This ensures that a society will have enough resources to treat everyone who get ill with COVID-19! Practice Social Distancing!


## Fun Animation

Now that we create the two curves, here is a fun little gif of the curves progressing everyday. It is funny that this was easier to implement than adding a bibliography to this website.


```{r,eval=F,echo=T}
library(gganimate)
library(gifski)
merge_tbl<- left_join(ode_tbl_1,ode_tbl_2,by="t") %>% left_join(y=ode_tbl_3,by="t")
merge_tbl_sub <- merge_tbl %>% filter(t %in% c(0:(2*365)))
gg.curve <- ggplot(data=merge_tbl_sub)+ 
              geom_line(aes(x=t,y=I.x,color="No Restriction"))+
              geom_line(aes(x=t,y=I.y,color="Moderate Restriction"))+
              geom_line(aes(x=t,y=I,color="Severe Restriction"))+
              theme_bw()+xlab("Time in Days")+ylab("Number of Individuals")+
              theme(legend.title = element_blank())
gg.gif<-gg.curve + transition_reveal(t)
animate(gg.gif, duration = 10, fps = 15, width = 600, height = 650, renderer = gifski_renderer())
anim_save("output.gif")
```

![](output.gif)

## References

